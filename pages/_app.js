import { createGlobalStyle } from "styled-components";
import { QueryClientProvider, QueryClient } from "react-query";
import { normalize } from "styled-normalize";

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <Global />
      <Component {...pageProps} />
    </QueryClientProvider>
  );
}

export default MyApp;

const Global = createGlobalStyle`
  ${normalize}
  html,
  body {
    padding: 0;
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
      Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }
`;
