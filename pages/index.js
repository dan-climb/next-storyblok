import NextHead from "next/head";
import Components from "../components/index";
import StoryblokService from "../utils/StoryblokService";
import React from "react";
import styled from "styled-components";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = { pageContent: props.page.data.story.content };
  }

  static async getInitialProps({ query }) {
    StoryblokService.setQuery(query);
    let slug = query.slug || "home";

    return {
      page: await StoryblokService.get(`cdn/stories/${slug}`),
    };
  }

  componentDidMount() {
    StoryblokService.initEditor(this);
  }

  render() {
    return (
      <>
        <NextHead>{StoryblokService.bridge()}</NextHead>
        <Main>{Components(this.state.pageContent)}</Main>
      </>
    );
  }
}

const Main = styled.main`
  > div {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
