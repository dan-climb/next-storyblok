import SbEditable from "storyblok-react";

export default function Teaser(props) {
  return (
    <SbEditable content={props.content}>
      <div className="teaser">{props.content.headline}</div>
    </SbEditable>
  );
}
