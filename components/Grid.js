import Components from "./index";
import SbEditable from "storyblok-react";

export default function Grid(props) {
  return (
    <SbEditable content={props.content}>
      <div className="grid">
        {props.content.columns.map((blok) => Components(blok))}
      </div>
    </SbEditable>
  );
}
