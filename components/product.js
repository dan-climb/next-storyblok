import styled from "styled-components";
import SbEditable from "storyblok-react";
import { useQuery } from "react-query";
import StoryblokService from "../utils/StoryblokService";

export default function Product({ content }) {
  const { product } = content;

  const { data } = useQuery([product], () =>
    StoryblokService.get(`cdn/stories`, { by_uuids: product })
  );

  const [item] = data?.data?.stories ?? [];
  const { images, name, description, price } = item?.content ?? {};

  return !item ? null : (
    <SbEditable content={content}>
      <Container>
        <ImageWrapper>
          <img src={images?.[0]?.filename} />
        </ImageWrapper>
        <Content>
          <Title>{name}</Title>
          <Description>
            {description?.content?.[0]?.content?.[0]?.text}
          </Description>
          <PriceContainer>
            <p>from just</p>
            <div>
              <Price>£{Number(price).toFixed(2)}</Price> <span>inc. vat</span>
            </div>
          </PriceContainer>
        </Content>
      </Container>
    </SbEditable>
  );
}

const Container = styled.div`
  max-width: 400px;
`;

const ImageWrapper = styled.div`
  max-height: 190px;
  overflow: hidden;

  img {
    width: 100%;
  }
`;

const Content = styled.div`
  padding: 0.5rem 1rem;
`;

const Title = styled.p`
  margin: 0.5rem 0 0;
  font-weight: 700;
  font-size: 1.2rem;
`;

const Description = styled.p`
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;

const PriceContainer = styled.div`
  text-transform: uppercase;
  font-weight: 300;
  font-size: 0.75rem;

  p {
    margin: 0;
  }
`;

const Price = styled.span`
  font-weight: 700;
  font-size: 1.2rem;
`;
