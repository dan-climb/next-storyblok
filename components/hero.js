import styled from "styled-components";
import SbEditable from "storyblok-react";

export default function Hero({ content }) {
  const { image, title, tagline, cta } = content;

  return (
    <SbEditable content={content}>
      <Container image={image.filename}>
        <h1>{title}</h1>
        <p>{tagline}</p>
        <button href={cta[0].link.url}>{cta[0].text}</button>
      </Container>
    </SbEditable>
  );
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 50vh;
  width: 100%;
  background: url(${({ image }) => image});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  h1 {
    font-size: 48px;
  }
`;
