import SbEditable from "storyblok-react";

export default function Feature(props) {
  return (
    <SbEditable content={props.content}>
      <div className="column feature">{props.content.name}</div>
    </SbEditable>
  );
}
