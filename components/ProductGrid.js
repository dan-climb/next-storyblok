import Components from "./index";
import SbEditable from "storyblok-react";
import styled from "styled-components";

export default function ProductGrid({ content }) {
  return (
    <SbEditable content={content}>
      <Grid>{content.products.map((blok) => Components(blok))}</Grid>
    </SbEditable>
  );
}

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 1rem;
  max-width: 1150px;
  width: fit-content;
  padding: 3rem 1rem;

  @media screen and (min-width: 600px) {
    grid-template-columns: repeat(2, 1fr);
  }
`;
